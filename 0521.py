import PyQt5
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QIcon,QFont ,QPixmap, QPalette,QColor,QIcon
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import vlc
import sys
import platform
import os ,time

#播放速率
vateList = [0.125, 0.25, 0.5, 2.0, 4.0, 8.0]

def pathWithName(nameStr):
    path1 = os.path.abspath('.')  # 表示当前所处的文件夹的绝对路径
    return path1 + '\\' + nameStr

class Ui_MainWindow(object):

    def setupUi(self, MainWindow):

        """带窗口图标"""
        self.setWindowIcon(QIcon(pathWithName('source\logo.jpg')))
        """选择文件"""
        self.textEdit = QTextEdit()
        self.setCentralWidget(self.textEdit)
        self.statusBar()
        self.openFile = QAction(QIcon('open.png'), '打开', self)
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('播放列表')
        fileMenu.addAction(self.openFile)

        """滑块控制音量"""
        # 创建一个水平的QSlider
        self.sld = QSlider(Qt.Horizontal, self)
        self.sld.setFocusPolicy(Qt.NoFocus)
        self.sld.setGeometry(410, 483, 80, 15)
        # 创建一个QLabel组件并给它设置一个静音图标
        self.label = QLabel(self)
        self.label.setPixmap(QPixmap(pathWithName('source\\volume.png')))
        print(pathWithName('source\\volume.jpg'))
        self.label.setGeometry(390, 483, 15, 16)

        """按钮"""
        self.okButton = QPushButton("开始", self)
        self.okButton.setGeometry(10,480,50,20)
        self.pausedButton = QPushButton("暂停",self)
        self.pausedButton.setGeometry(10,480,50,20)
        self.sforwardButton = QPushButton("快进", self)
        self.sforwardButton.setGeometry(60,480,50,20)
        self.sbackwardButton = QPushButton("快退", self)
        self.sbackwardButton.setGeometry(110, 480, 50, 20)

        """进度条"""
        # 新建一个QProgressBar构造器
        self.pbar = QProgressBar(self)
        self.pbar.setGeometry(10, 470, 495, 10)

        """倍速播放"""
        # 速率变化下拉框 实例化QComBox对象
        self.cb = QComboBox(self)
        self.cb.setGeometry(160,480,70,20)
        itemList = []
        for value in vateList:
            itemList.append(str(value) + 'X')
        self.cb.addItems(itemList)

        """创建窗口"""
        MainWindow.setObjectName("MainWindow")

        MainWindow.resize(500,500)#resize()方法能改变控件的大小

        self.centralwidget = QtWidgets.QWidget(MainWindow)

        self.centralwidget.setObjectName("centralwidget")

        self.horizontalLayout = QtWidgets.QHBoxLayout(self.centralwidget)

        self.horizontalLayout.setObjectName("horizontalLayout")

        self.frame = QtWidgets.QFrame(self.centralwidget)

        self.frame.setStyleSheet("background-color: rgb(0, 0, 0);")

        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)

        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)

        self.frame.setObjectName("frame")

        self.horizontalLayout.addWidget(self.frame)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    #暂停事件
    def addPausedBtnAction(self, event):
        self.pausedButton.clicked.connect(event)
    #速率事件
    def addComoboxAction(self, event):
        self.cb.currentIndexChanged.connect(event)
    #音量事件
    def volumeAction(self,event):
        self.sld.valueChanged[int].connect(event)
    #快进事件
    def addSForwardBtnAction(self, event):
        self.sforwardButton.clicked.connect(event)
    #快退事件
    def addSBackwardBtnAction(self, event):
        self.sbackwardButton.clicked.connect(event)

    def updatePBarValue(self, value):
        print('value======'+ str(value))
        self.pbar.setValue(value)

    def updatePauseBtnText(self, state):
        if state == 0:
            self.pausedButton.setText('暂停')
        else:
            self.pausedButton.setText('播放')

    def addOpenFileAction(self,event):
        self.openFile.triggered.connect(event)

    """消息盒子"""
    def closeEvent(self, event):
        """我们创建了一个消息框，上面有俩按钮：Yes和No.
        第一个字符串显示在消息框的标题栏，第二个字符串显示在对话框，
        第三个参数是消息框的俩按钮，最后一个参数是默认按钮，这个按钮是默认选中的。返回值在变量reply里"""

        reply = QMessageBox.question(self, 'Message',
            "确定要退出吗?", QMessageBox.Yes |
            QMessageBox.No, QMessageBox.No)
        #这里判断返回值，如果点击的是Yes按钮，我们就关闭组件和应用，否者就忽略关闭事件
        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

    def retranslateUi(self, MainWindow):

        _translate = QtCore.QCoreApplication.translate

        MainWindow.setWindowTitle(_translate("MainWindow", "华强方特视频播放器"))#我们给这个窗口添加了一个标题，标题在标题栏展示

class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self,*args,parent=None):
        super(MainWindow,self).__init__(parent)
        self.setupUi(self)
        if args:
            self.instance = vlc.Instance(*args)
            self.mediaplayer =self.instance.media_player_new()
        else:
            self.mediaplayer = vlc.MediaPlayer()
            self.palette=self.frame.palette()
            self.palette.setColor(QPalette.Window,QColor(0,0,0))
            self.frame.setPalette(self.palette)
            self.frame.setAutoFillBackground(True)
        if platform.system() =='Windows':
            self.mediaplayer.set_hwnd(self.frame.winId())
        else:
            self.mediaplayer.set_xwindow(self.frame.winId())

    def playPath(self,path):
        self.mediaplayer.set_mrl(path)
        self.mediaplayer.play()
    # 暂停
    def pause(self):
        self.mediaplayer.pause()
    # 设置播放速率（如：1.2，表示加速1.2倍播放）
    def set_rate(self, rate):
        return self.mediaplayer.set_rate(rate)
    # 设置音量（0~100）
    def set_volume(self, volume):
        return self.mediaplayer.audio_set_volume(volume)
    # 前进1秒
    def forward(self):
        """Go forward one sec"""
        self.mediaplayer.set_time(self.mediaplayer.get_time() + 1000)
    # 后退1秒
    def backward(self):
        """Go backward one sec"""
        self.mediaplayer.set_time(self.mediaplayer.get_time() - 1000)
    # 注册监听器
    def add_callback(self, event_type, callback):
        self.mediaplayer.event_manager().event_attach(event_type, callback)
    # 移除监听器
    def remove_callback(self, event_type, callback):
        self.mediaplayer.event_manager().event_detach(event_type, callback)
    # 返回当前状态：正在播放；暂停中；其他
    def get_state(self):
        state = self.mediaplayer.get_state()
        if state == vlc.State.Playing:
            return 1
        elif state == vlc.State.Paused:
            return 0
        else:
            return -1

    def timeChanged(self,event):
        time = self.mediaplayer.get_time()
        self.updatePBarValue(int(time / 1000))
        print("time changed=="+ str(time))

    def positionChanged(self,event):
        position = self.mediaplayer.get_position()
        print("position changed"+str(position) )

    def playerPlaying(self, event):
        maxTime = self.mediaplayer.get_length()
        # self.setPbarMaxValue(int(maxTime/ 1000) + 1)
        print('maxTime=====' + str(maxTime / 1000))

    """选择文件"""
    def showDialog(self):
        # 弹出QFileDialog窗口。getOpenFileName()方法的第一个参数是说明文字，第二个参数是默认打开的文件夹路径。默认情况下显示所有类型的文件
        fname = QFileDialog.getOpenFileName(self, '选择文件', '', 'video files(*.mp4 , *.webm , *.mkv)')
        self.playPath(fname[0])

    #把需要添加callback的代码拉出来写一个函数
    def addPlayerCallBack(self):
        self.add_callback(vlc.EventType.MediaPlayerTimeChanged, self.timeChanged)
        self.add_callback(vlc.EventType.MediaPlayerPositionChanged, self.positionChanged)
        self.add_callback(vlc.EventType.MediaPlayerPlaying, self.playerPlaying)

    def pausedBtnAction(self):
        print('播放暂停')
        if self.get_state() == 1:
            self.pause()
            self.updatePauseBtnText(1)
        else:
            self.pause()
            self.updatePauseBtnText(0)

    def comoboxAction(self,i):
        if i < len(vateList):
            vate = vateList[i]
            print("速率=" + str(vate))
            ui.set_rate(vate)

    def volomeChangedAction(self,value):
        print('音量变换=' + str(value))
        ui.set_volume(value)

    def sforwardBtnAction(self):
        print('快进')
        ui.forward()

    def sbackBtnAction(self):
        print('快退')
        ui.backward()

    def addUIEvent(self):
        self.addPausedBtnAction(self.pausedBtnAction)
        self.addComoboxAction(self.comoboxAction)
        self.volumeAction(self.volomeChangedAction)
        self.addSForwardBtnAction(self.sforwardBtnAction)
        self.addSBackwardBtnAction(self.sbackBtnAction)
        self.addOpenFileAction(self.showDialog)

if __name__ =='__main__':
    import sys
    """每个PyQt5应用都必须创建一个应用对象。sys.argv是一组命令行参数的列表"""
    app = QtWidgets.QApplication(sys.argv)
    ui = MainWindow()
    ui.show()#show()能让控件在桌面上显示出来。控件在内存里创建，之后才能在显示器上显示出来

    ui.addPlayerCallBack()
    ui.addUIEvent()
    # 这是csy添加的代码
    """最后，我们进入了应用的主循环中，事件处理器这个时候开始工作。主循环从窗口上接收事件，并把事件传入到派发到应用控件里。
    当调用exit()方法或直接销毁主控件时，主循环就会结束。sys.exit()方法能确保主循环安全退出。外部环境能通知主控件怎么结束。
    exec_()之所以有个下划线，是因为exec是一个Python的关键字"""


    # 这是csy添加的分支代码
    sys.exit(app.exec_())

